const orderProduct = (product) => (dispatch) => {
	dispatch({
		type: 'ORDER_PRODUCT',
		product: product
	})
}
export default orderProduct;