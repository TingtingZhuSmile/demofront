const initialState = {
	orders: []
}

export default (state = initialState, action) => {

	switch (action.type) {
		case 'ORDER_PRODUCT':
			const orders = state.orders;
			// eslint-disable-next-line no-unused-expressions
			const index = orders.findIndex((product) => {
				return product.productName === action.product.productName
			});
			index ? orders.push({
				productName: action.product.productName,
				price: action.product.price,
				unit: action.product.unit,
				url: action.product.url,
				amount: 1
			}) : orders[index].amount += 1;
			console.info(orders);

			return {
				...state,
				orders: orders
			};
		default:
			return state
	}

}