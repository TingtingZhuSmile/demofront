import React from "react";
import {bindActionCreators} from "redux";
import orderProduct from "./itemAction";
import {connect} from "react-redux";

class Item extends React.Component {


	// eslint-disable-next-line no-useless-constructor
	constructor(props, context) {
		super(props, context);
	}

	static onClickHandler() {
		this.props.orderProduct(this.props.product);
		// console.log(this.props.product);
		// console.log("ok");
	}

	render() {
		return <li key={this.props.index}>
			<h1>{this.props.product.productName}</h1>
			<p>{`单价：${this.props.product.price}元/${this.props.product.unit}`}</p>
			<button onClick={Item.onClickHandler.bind(this)}>+</button>
		</li>;
	}
}

const mapStateToProps = state => ({
	products: state.orderProduct.orders
});
const mapDispatchToProps = dispatch => bindActionCreators({orderProduct}, dispatch);
export default connect(mapStateToProps,mapDispatchToProps)(Item);