import {combineReducers} from "redux";
import shopReducer from "./shop/reducers/shopReducer";
import addProductReducers from "./addProduct/reducers/addProductReducers";
import itemReducer from "./item/itemReducer";

const rootReducer = combineReducers({
	shopReducer: shopReducer,
	addProductReduer:addProductReducers,
	orderProduct:itemReducer
	}

);
export default rootReducer;