const initialState = {
		productName: null,
		price: null,
		unit: null,
		url: null
	}
;

export default (state = initialState, action) => {
	switch (action.type) {
		case 'INPUT_PRODUCT_NAME':
			return {
				...state,
				productName: action.productName
			};
		case 'INPUT_PRODUCT_PRICE':
			return {
				...state,
				price: action.price
			};
		case 'INPUT_PRODUCT_UNIT':
			return {
				...state,
				unit: action.unit
			};
		case 'INPUT_PRODUCT_URL':
			return {
				...state,
				url: action.url
			};
		case 'COMMIT_INPUT':
		default:
			return state
	}

}
