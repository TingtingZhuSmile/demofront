export const inputProductName=(name)=>(dispatch)=>{
	dispatch({
		type:'INPUT_PRODUCT_NAME',
		productName: name
	})
}
export const inputProductPrice=(price)=>(dispatch)=>{
	dispatch({
		type:'INPUT_PRODUCT_PRICE',
		price: price
	})
}

export const inputProductUnit=(unit)=>(dispatch)=>{
	dispatch({
		type:'INPUT_PRODUCT_UNIT',
		unit: unit
	})
}

export const inputProductUrl=(url)=>(dispatch)=>{
	dispatch({
		type:'INPUT_PRODUCT_URL',
		url: url
	})
}

export const addProduct = ()=>(dispatch,getState)=>{
	const content = getState().addProductReduer;
	console.log(content);
	fetch(`http://localhost:8080/api/products`, ProductInfomation(content)).then().then(dispatch({
		type: 'COMMIT_INPUT'
	}));
}


const ProductInfomation = (content) => {
	return {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify(content)

	}
};
