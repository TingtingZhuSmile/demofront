import React from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {getProducts} from "../../shop/actions/shopAction";
import {bindActionCreators} from "redux";
import {
	addProduct,
	inputProductName,
	inputProductPrice,
	inputProductUnit,
	inputProductUrl
} from "../actions/addProductActions";

class AddProduct extends React.Component {

	// eslint-disable-next-line no-useless-constructor
	constructor(props, context) {
		super(props, context);
	}

	inputNameHandler(event) {
		this.props.inputProductName(event.target.value);
	}

	inputPriceHandler(event) {
		this.props.inputProductPrice(event.target.value);
	}

	inputUnitHandler(event) {
		console.info(event.target.value)
		this.props.inputProductUnit(event.target.value);
	}

	inputUrlHandler(event) {
		this.props.inputProductUrl(event.target.value);
	}

	onclickHandler() {
		this.props.addProduct()
	}

	render() {
		return (
			<div className="addProduct">

				<h1>添加商品</h1>
				<ul>
					<li>
						<p>*名称</p>
						<input defaultValue='名称' onChange={this.inputNameHandler.bind(this)}/>
					</li>
					<li>
						<p>*价格</p>
						<input defaultValue='价格' onChange={this.inputPriceHandler.bind(this)}/></li>
					<li>
						<p>*单位</p>
						<input defaultValue='单位' onChange={this.inputUnitHandler.bind(this)}/></li>
					<li>
						<p>*图片</p>
						<input defaultValue='图片' onChange={this.inputUrlHandler.bind(this)}/></li>
				</ul>
				<button onClick={this.onclickHandler.bind(this)}> 提交</button>
			</div>
		)
	}
}

const mapStateToProps = state => (
	{
		productName: state.addProductReduer.name,
		price: state.addProductReduer.price,
		unit: state.addProductReduer.unit,
		url: state.addProductReduer.url
	}
);

const mapDisptachToProps = dispatch => bindActionCreators({
	inputProductName, inputProductPrice, inputProductUnit,
	inputProductUrl, addProduct
}, dispatch);


export default connect(mapStateToProps, mapDisptachToProps)(AddProduct);
