import React from "react";
import {connect} from "react-redux";
import OrderItem, {getOrderItem} from "./orderItem";

function getOrders() {
	return (
			this.props.orders.map(order => {
				return <OrderItem order={order}></OrderItem>
			})
	);
}

class Orders extends React.Component {
	render(){
		console.log(this.props.orders);
		return (
			<p>
				hello
				<table border="1">
					<tr>
						<th>名字</th>
						<th>单价</th>
						<th>数量</th>
						<th>单位</th>
						<th>操作</th>
					</tr>
					{getOrders.call(this)}
				</table>
			</p>


		)
	}

}

const mapStateToProps = state => ({
	orders: state.orderProduct.orders
});

export default connect(mapStateToProps)(Orders);