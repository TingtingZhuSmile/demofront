import React from "react";

class OrderItem extends React.Component{


	constructor(props, context) {
		super(props, context);
	}

	render(){
		const order = this.props.order;
		return(
		<tr>
			<td>{order.productName}</td>
			<td>{order.price}</td>
			<td>{order.amount}</td>
			<td>{order.unit}</td>
			<td>
				<button>删除</button>
			</td>
		</tr>);
	}
}

export default OrderItem;