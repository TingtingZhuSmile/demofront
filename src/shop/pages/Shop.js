import React from "react";
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import {getProducts} from "../actions/shopAction";
import {connect} from "react-redux";
import {returnProducts} from "../../returnProducts";


class Shop extends React.Component{


	// eslint-disable-next-line no-useless-constructor
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.props.getProducts();
	}

	render(){

		const products = this.props.products;
		console.log(products.length);
		return (
			<div className="storeHomePage">
				<div>
					{returnProducts(products)}
				</div>

			</div>
		);
	}

}


const mapStateToProps = state => ({
	products: state.shopReducer.products
});

const mapDisptachToProps = dispatch=>bindActionCreators({getProducts},dispatch);


export default connect(mapStateToProps,mapDisptachToProps)(Shop);
