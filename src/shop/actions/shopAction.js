export  function getProducts() {

	return function (dispatch) {

		return fetch(`http://localhost:8080/api/products`)
			.then(
				response => response.json(),
				error => console.log('Error occurred.', error)
			)
			.then(json =>
				dispatch({
					type: 'GET_PRODUCTS',
					products: json
				}))
	}
}
