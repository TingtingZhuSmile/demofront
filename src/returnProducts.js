import React from "react";
import Item from "./item/item";


export function returnProducts(products) {
	return products.length === 0 ? '' :
		products.map((product, index) => {
			return (
				<div>
					<Item index={index} product={product}/>
				</div>
			)
		});
}