import React from 'react';
import logo from './logo.svg';
import './App.css';
import './App.less';
import {BrowserRouter, Link} from "react-router-dom";
import Shop from "./shop/pages/Shop";
import {Route} from "react-router";
import Provider from "react-redux/es/components/Provider";
import store from "./store";
import AddProduct from "./addProduct/pages/AddProduct";
import Orders from "./orders/Orders";

function App() {
	return (
		<Provider store={store}>
		<div className="App">
			<BrowserRouter>

				<header className='navHeader'>
					<ul >
						<li><Link to="/">商城</Link></li>
						<li><Link to="/products/add">添加商品</Link></li>
						<li><Link to="/orders">订单</Link></li>
					</ul>
				</header>

				<Route path="/products/add" component={AddProduct}></Route>
				<Route path="/orders" component={Orders}></Route>
				<Route exact path="/" component={Shop}></Route>


			</BrowserRouter>


		</div>
		</Provider>
	);
}

export default App;
